package com.test.facebooklogin;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import org.json.JSONObject;

import com.androidquery.AQuery;
import com.facebook.Session;
import com.facebook.Session.StatusCallback;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.AsyncFacebookRunner.RequestListener;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class SecondPage extends Activity implements OnClickListener {

	private Facebook facebook;
	private AsyncFacebookRunner mAsyncRunner;
	Context context;
	StatusCallback statusCallback;

	// Reference of BUTTON

	Button facebookPostButton, logOutButton;
	TextView userName, email, gender;
	EditText input;
	EditText facebookPostEditText, profileUserName, profileEmail,
			profileGender;
	private AQuery aq;
	String message, data, user_Name, getEmail, getGender, getId, url;
	JSONObject jsonObj;
	ImageView profile_pic;

	private static final String[] PERMISSIONS = new String[] { "public_profile" };
	private static final String TOKEN = "access_token";
	private static final String EXPIRES = "expires_in";
	private static final String KEY = "facebook-credentials";
	private String messageToPost;
	private Editor editor;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.second_page);
		context = this;

		facebook = new Facebook(getString(R.string.app_id));
		mAsyncRunner = new AsyncFacebookRunner(facebook);
		aq = new AQuery(context);

		facebookPostButton = (Button) findViewById(R.id.facebookPostButton);
		logOutButton = (Button) findViewById(R.id.logoutbutton);
		profile_pic = (ImageView) findViewById(R.id.profileimageView1);

		profileEmail = (EditText) findViewById(R.id.emaileditText1);
		profileUserName = (EditText) findViewById(R.id.userNameeditText1);
		profileGender = (EditText) findViewById(R.id.gendereditText1);

		profileEmail.setVisibility(View.INVISIBLE);
		profileGender.setVisibility(View.INVISIBLE);
		profileUserName.setVisibility(View.INVISIBLE);

		facebookPostButton.setOnClickListener(this);
		logOutButton.setOnClickListener(this);

		Intent intent2 = getIntent();
		data = intent2.getStringExtra("recvd_data");

		parse();

		Toast.makeText(context, data, Toast.LENGTH_LONG).show();
	}

	@SuppressWarnings("deprecation")
	public boolean saveCredentials(Facebook facebook) {
		editor = getApplicationContext().getSharedPreferences(KEY,
				Context.MODE_PRIVATE).edit();
		editor.putString(TOKEN, facebook.getAccessToken());
		editor.putLong(EXPIRES, facebook.getAccessExpires());
		return editor.commit();
	}

	@SuppressWarnings("deprecation")
	public boolean restoreCredentials(Facebook facebook) {
		SharedPreferences sharedPreferences = getApplicationContext()
				.getSharedPreferences(KEY, Context.MODE_PRIVATE);
		facebook.setAccessToken(sharedPreferences.getString(TOKEN, null));
		facebook.setAccessExpires(sharedPreferences.getLong(EXPIRES, 0));
		return facebook.isSessionValid();
	}

	@SuppressWarnings("deprecation")
	public void loginAndPostToWall() {

		facebook.authorize(this, PERMISSIONS, new DialogListener() {

			@Override
			public void onComplete(Bundle values) {
				// TODO Auto-generated method stub

				saveCredentials(facebook);
				if (messageToPost != null) {
					postToWall(messageToPost);
				}

			}

			@Override
			public void onFacebookError(FacebookError e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onError(DialogError e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onCancel() {
				// TODO Auto-generated method stub

			}
		});
	}

	@SuppressWarnings("deprecation")
	public void postToWall(String message) {
		final Bundle parameters = new Bundle();
		parameters.putString("message", message);
		parameters.putString("description", "topic share");
		try {
			mAsyncRunner.request("me", new RequestListener() {

				@Override
				public void onMalformedURLException(MalformedURLException e,
						Object state) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onIOException(IOException e, Object state) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onFileNotFoundException(FileNotFoundException e,
						Object state) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onFacebookError(FacebookError e, Object state) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onComplete(String response, Object state) {
					// TODO Auto-generated method stub
					try {
						facebook.request("me");
						response = facebook.request("me/feed", parameters,
								"POST");
						Log.d("Tests", "got response: " + response);

						if (response == null || response.equals("")
								|| response.equals("false")) {
							Toast.makeText(context, "Blank Response",
									Toast.LENGTH_LONG).show();
						} else {

							SecondPage.this.runOnUiThread(new Runnable() {

								@Override
								public void run() {
									// TODO Auto-generated method stub
									Toast.makeText(
											context,
											"Message posted to your facebook wall!",
											Toast.LENGTH_LONG).show();
								}
							});

						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

		} catch (Exception e) {
			showToast("Failed to post to wall!");
			e.printStackTrace();
		}
	}

	private void showToast(String message) {
		// Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT)
		// .show();
	}

	public void parse() {

		try {

			// load an image to an ImageView from network, cache image to file
			// and memory

			jsonObj = new JSONObject(data);
			user_Name = jsonObj.optString("name").toString();
			getEmail = jsonObj.optString("email").toString();
			getGender = jsonObj.optString("gender").toString();
			getId = jsonObj.optString("id").toString();

			url = "http://graph.facebook.com/" + getId + "/picture?type=large";

			aq.id(profile_pic).image(url);

			profileUserName.setText(user_Name);
			profileEmail.setText(getEmail);
			profileGender.setText(getGender);

			profileEmail.setVisibility(View.VISIBLE);
			profileGender.setVisibility(View.VISIBLE);
			profileUserName.setVisibility(View.VISIBLE);
		} catch (Exception e) {

			e.printStackTrace();

		}

	}

	@SuppressWarnings("deprecation")
	public void logoutFromFacebook() {

		Session session = Session.getActiveSession();
		if (session != null) {

			if (!session.isClosed()) {
				session.closeAndClearTokenInformation();
				// clear your preferences if saved

				// editor.clear();
				// editor.commit();

				Intent intent = new Intent(SecondPage.this, MainActivity.class);

				startActivity(intent);
				finish();
			} else {

				session.closeAndClearTokenInformation();

				Intent intent = new Intent(SecondPage.this, MainActivity.class);

				startActivity(intent);
				finish();

			}
		} else {

			session = new Session(context);
			Session.setActiveSession(session);

			session.closeAndClearTokenInformation();
			// clear your preferences if saved

			// editor.clear();
			// editor.commit();

			Intent intent = new Intent(SecondPage.this, MainActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

			startActivity(intent);
			finish();
		}

	}

	public void post() {

		restoreCredentials(facebook);
		// messageToPost = "Test From Android App";

		message = input.getText().toString();
		messageToPost = input.getText().toString();

		// message = facebookPostEditText.getText().toString();
		// messageToPost = facebookPostEditText.getText().toString();

		if (message.equals("") && messageToPost.equals("")) {

			Toast.makeText(context, "Post is Empty", Toast.LENGTH_LONG).show();

		} else {

			if (!facebook.isSessionValid()) {
				loginAndPostToWall();
			} else {
				postToWall(messageToPost);
			}
		}

	}

	@SuppressWarnings("deprecation")
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {
		case R.id.facebookPostButton:
			// handle LOGIN click;

			LayoutInflater layoutInflater = LayoutInflater.from(context);
			View promptView = layoutInflater.inflate(R.layout.prompt_dialog,
					null);
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					context);

			// set prompts.xml to be the layout file of the alertdialog builder

			alertDialogBuilder.setView(promptView);

			input = (EditText) promptView.findViewById(R.id.userInput);

			// setup a dialog window

			alertDialogBuilder
					.setCancelable(false)
					.setPositiveButton("Publish",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub

									post();

									// facebookPostEditText.setText(input.getText());

								}

							})
					.setNegativeButton("NO",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub
									dialog.cancel();
								}
							});

			// create an alert dialog
			AlertDialog alertD = alertDialogBuilder.create();
			alertD.show();
			break;

		case R.id.logoutbutton:
			// handle LOGOUT click;

			logoutFromFacebook();
			break;
		}

	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		facebook.authorizeCallback(requestCode, resultCode, data);
	}

}
