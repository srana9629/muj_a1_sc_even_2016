package in.forsk.listactivitywithcursoradapter_sqlite.DbAdapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * 
 * @author dev A helper class to manage database creation and version
 *         management.
 *
 */
public class DBAdapter extends SQLiteOpenHelper {

	// SQLiteDatabase has methods to create, delete, execute SQL commands, and
	// perform other common database management tasks.
	private SQLiteDatabase myDataBase;
	static public DBAdapter instance;

	// public static final String DATABASE_FILE_PATH =
	// Environment.getExternalStorageDirectory();;
	private static final int DATABASE_VERSION = 1;
	// Database Name
	private static final String DATABASE_NAME = "forsk.db";

	public static final String FACULTY_TABLE_NAME = "FACULTY";
	public static final String FACULTY_COLUMN_ID = "_id";
	public static final String FACULTY_COLUMN_FIRSTNAME = "first_name";
	public static final String FACULTY_COLUMN_LASTNAME = "last_name";
	public static final String FACULTY_COLUMN_PHOTO = "photo";
	public static final String FACULTY_COLUMN_DEP = "department";
	public static final String FACULTY_COLUMN_PHONE = "phone";
	public static final String FACULTY_COLUMN_EMAIL = "email";

	// we need to maintain a single instance of the database connection class
	// since multipal threads will be using this connection
	public static DBAdapter getInstance(Context context) {
		if (instance == null) {
			instance = new DBAdapter(context);
		}
		return instance;
	}

	private DBAdapter(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// super(context, Environment.getExternalStorageDirectory() +
		// File.separator + DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_TABLE_FACULTY = "CREATE TABLE " + FACULTY_TABLE_NAME + "(" + FACULTY_COLUMN_ID + " INTEGER PRIMARY KEY," + FACULTY_COLUMN_FIRSTNAME + " TEXT," + FACULTY_COLUMN_LASTNAME
				+ " TEXT," + FACULTY_COLUMN_PHOTO + " TEXT," + FACULTY_COLUMN_DEP + " TEXT," + FACULTY_COLUMN_PHONE + " TEXT," + FACULTY_COLUMN_EMAIL + " TEXT)";
		db.execSQL(CREATE_TABLE_FACULTY);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		// on upgrade drop older tables
		db.execSQL("DROP TABLE IF EXISTS " + FACULTY_TABLE_NAME);
		// create new tables
		onCreate(db);
	}

	/**
	 * This function used to select the records from DB.
	 * 
	 * @param tableName
	 * @param tableColumns
	 * @param whereClase
	 * @param whereArgs
	 * @param groupBy
	 * @param having
	 * @param orderBy
	 * @return A Cursor object, which is positioned before the first entry.
	 */
	public Cursor selectRecordsFromDB(String tableName, String[] tableColumns, String whereClase, String whereArgs[], String groupBy, String having, String orderBy) {

		myDataBase = this.getWritableDatabase();

		return myDataBase.query(tableName, tableColumns, whereClase, whereArgs, groupBy, having, orderBy);
	}

	/**
	 * This function used to insert the Record in DB.
	 * 
	 * @param tableName
	 * @param nullColumnHack
	 * @param initialValues
	 * @return the row ID of the newly inserted row, or -1 if an error occurred
	 */
	public long insertRecordsInDB(String tableName, String nullColumnHack, ContentValues initialValues) {

		myDataBase = this.getWritableDatabase();

		return myDataBase.insert(tableName, nullColumnHack, initialValues);
	}

	/**
	 * This function used to update the Record in DB.
	 * 
	 * @param tableName
	 * @param initialValues
	 * @param whereClause
	 * @param whereArgs
	 * @return true / false on updating one or more records
	 */
	public boolean updateRecordInDB(String tableName, ContentValues initialValues, String whereClause, String whereArgs[]) {

		myDataBase = this.getWritableDatabase();

		return myDataBase.update(tableName, initialValues, whereClause, whereArgs) > 0;
	}

	/**
	 * This function used to update the Record in DB.
	 * 
	 * @param tableName
	 * @param initialValues
	 * @param whereClause
	 * @param whereArgs
	 * @return 0 in case of failure otherwise return no of row(s) are updated
	 */
	public int updateRecordsInDB(String tableName, ContentValues initialValues, String whereClause, String whereArgs[]) {
		myDataBase = this.getWritableDatabase();

		return myDataBase.update(tableName, initialValues, whereClause, whereArgs);
	}

	/**
	 * This function used to delete the Record in DB.
	 * 
	 * @param tableName
	 * @param whereClause
	 * @param whereArgs
	 * @return 0 in case of failure otherwise return no of row(s) are deleted.
	 */
	public int deleteRecordInDB(String tableName, String whereClause, String[] whereArgs) {

		myDataBase = this.getWritableDatabase();

		return myDataBase.delete(tableName, whereClause, whereArgs);
	}

	// --------------------- Select Raw Query Functions ---------------------

	/**
	 * apply raw Query
	 * 
	 * @param query
	 * @param selectionArgs
	 * @return Cursor
	 */
	public Cursor selectRecordsFromDB(String query, String[] selectionArgs) {
		myDataBase = this.getWritableDatabase();

		return myDataBase.rawQuery(query, selectionArgs);
	}

	/**
	 * apply raw query and return result in list
	 * 
	 * @param query
	 * @param selectionArgs
	 * @return ArrayList<ArrayList<String>>
	 */
	public ArrayList<ArrayList<String>> selectRecordsFromDBList(String query, String[] selectionArgs) {

		myDataBase = this.getWritableDatabase();

		ArrayList<ArrayList<String>> retList = new ArrayList<ArrayList<String>>();

		ArrayList<String> list = new ArrayList<String>();
		Cursor cursor = myDataBase.rawQuery(query, selectionArgs);

		if (cursor.moveToFirst()) {
			do {
				list = new ArrayList<String>();
				for (int i = 0; i < cursor.getColumnCount(); i++) {
					list.add(cursor.getString(i));
				}
				retList.add(list);
			} while (cursor.moveToNext());
		}
		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
		}
		return retList;
	}

}
